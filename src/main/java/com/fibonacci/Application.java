package com.fibonacci;

        import java.util.Scanner;
        import java.util.ArrayList;
/**
 * Class Application consist all needed methods.
 *
 * @author Ihor Chushchak
 * @version 1.0
 */
public class Application {
    /**
     * Prints ordinary arrays.
     *
     * @param interval Array to do with.
     */
    private static void showInterval(int[] interval){
        for (int i = 0; i < interval.length ; i++) {
            System.out.print(" " + interval[i]);
        }
    }
    /**
     * Prints arrayList's.
     *
     * @param arrayList Array to do with.
     */
    private static void showIntervalList(ArrayList<Integer> arrayList){
        for(int i = 0; i<arrayList.size(); i++) {
            Integer value = arrayList.get(i);
            System.out.print(" " + value);}
    }
    /**
     * @param start set first number of interval.
     * @param end set last number of interval.
     * @return initialized interval from the range.
     */
    private static int[] setInterval(int start, int end){
        int intervalLenth = (end - start + 1);
        int interval[] = new int[intervalLenth];
        for (int i = 1; i < intervalLenth ; i++) {
            interval[0] = start;
            interval[i] = interval[i-1]+1;
        }
        return interval;
    }
    /**
     * Finds Odd numbers from the interval.
     *
     * @param interval Array to do with.
     * @return ArrayList with odd numbers.
     */
    private static ArrayList getOdd(int[] interval) {
        ArrayList<Integer> oddInterval = new ArrayList<Integer>();
        for (int i = 0; i < interval.length; i++) {
            if ((interval[i] % 2) != 0) {
                oddInterval.add(interval[i]);
            }
        }
        return oddInterval;
    }
    /**
     * Finds Even numbers from the interval.
     *
     * @param interval Array with to do with.
     * @return ArrayList with even numbers.
     */
    private static ArrayList getEven(int[] interval) {
        ArrayList evenInterval = new ArrayList<Integer>();
        for (int i = interval.length - 1; i >= 0; i--) {
            if ((interval[i] % 2) == 0) {
                evenInterval.add(interval[i]);
            }
        }
        return evenInterval;
    }
    /**
     * Finds the sum of numbers from the range.
     *
     * @param arrayList Array with to do with.
     * @return sum of the Array.
     */
    private static int getSum(ArrayList<Integer> arrayList){
        int sum = 0;
        for(int i = 0; i<arrayList.size(); i++) {
            Integer value = arrayList.get(i);
            sum += value;}
        return sum;
    }
    /**
     * Builds Fibonacci number starting with
     * chosen F1(max odd number) and F2(max even number) numbers.
     *
     * @param size Index of fibonacci numbers.
     * @param arrayListOdd Array with odd numbers.
     * @param arrayListEven Array with even numbers.
     * @return An integer array fibonacci with length "size" fibonacci number.
     */
    private static int[] getFibonacci(int size , ArrayList<Integer> arrayListOdd, ArrayList<Integer> arrayListEven){
        int[] fibonacci = new int[size];
        /* 1 extra to handle case, size = 0 */
        if(size==0){
            fibonacci = new int[size + 2];
        }
        else if(size==1){
            fibonacci = new int[size + 1];
        }
        else if(size==2){
            fibonacci = new int[size];
        }
        /* Sets maximum odd number from the range */
        int F1 = arrayListOdd.get(arrayListOdd.size() - 1);
        /* Sets maximum even number from the range */
        int F2 = arrayListEven.get(0);
        /* 0th and 1st number of the series are F1 and F2*/
        fibonacci[0] = F1;
        fibonacci[1] = F2;
        for (int i = 2; i < size; i++)
        {
            /* Add the previous 2 numbers in the series and store it */
            fibonacci[i] = fibonacci[i-1] + fibonacci[i-2];
        }

        return fibonacci;
    }
    /**
     * Prints the percentage of odd and even fibonacci numbers.
     *
     * @param fibonacci Array to do with.
     */
    private static void getPercentage(int[] fibonacci){
        int numOfOdd = 0;
        for (int i = 0; i < fibonacci.length; i++) {
            if ((fibonacci[i] % 2) != 0) {
                numOfOdd++;
            }
        }
        /* Finds percentage for odd numbers */
        float percentageOdd = 100 * numOfOdd ;
        percentageOdd = percentageOdd/fibonacci.length;
        System.out.println("Percentage of odd Fibonacci numbers: " + percentageOdd + "%");
        /* Finds percentage for even numbers */
        float percentageEven = 100 - percentageOdd;
        System.out.println("Percentage of odd Fibonacci numbers: " + percentageEven + "%");
    }
    /**
     * Uses all methods above.
     *
     * @param args Command line arguments.
     */
    public static void main(String[] args) {
        /**
         * Represents the scanner for all variables in the main.
         */
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please, enter first number of the interval: ");
        int start = scanner.nextInt();
        System.out.print("Please, enter last number of the interval: ");
        int end = scanner.nextInt();
        if (end < start) {
            try {
                throw new OutOfIntervalExeption();
            } catch (OutOfIntervalExeption e) {
                e.showMessage();
            }
        } else {
        System.out.print("Interval itself:");
        showInterval(setInterval(start,end));
        System.out.println();
        System.out.print("Odd numbers array:");
        showIntervalList(getOdd(setInterval(start,end)));
        System.out.println();
        System.out.print("Even numbers array:");
        showIntervalList(getEven(setInterval(start,end)));
        System.out.println();
        System.out.println("Sum of the odd numbers is - " + getSum(getOdd(setInterval(start,end))));
        System.out.println("Sum of the even numbers is - " + getSum(getEven(setInterval(start,end))));
        System.out.print("Please, enter size of the fibonacci numbers: ");
        int size = scanner.nextInt();
        System.out.print("Fibonacci numbers");
        showInterval(getFibonacci(size,getOdd(setInterval(start,end)),getEven(setInterval(start,end))));
        System.out.println();
        getPercentage(getFibonacci(size,getOdd(setInterval(start,end)),getEven(setInterval(start,end))));}
    }
}
